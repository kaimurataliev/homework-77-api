const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (rq, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage: storage});

const createRouter = (db) => {
    router.get('/', (req, res) => {
        res.send(db.getData());
    });

    router.post('/', upload.single('image'), (req, res) => {

        if(!req.body.author) {
            req.body.author = 'Anonymous';
        }

        const message = req.body;

        if(req.file) {
            message.image = req.file.filename;
        }

        if(!req.body.message) {
            res.status(400).send({error: "You have to enter message!"})
        } else {

            db.postMessage(message).then(result => {
                res.send(result);
            });
        }

    });

    return router;
};

module.exports = createRouter;